const Order = require("../models/Order");
const Product = require('../models/Product');
const User = require('../models/User');

// Creating Orders
module.exports.createOrder = async (req, res) => {
  try {
    const { userId, products } = req.body;

    // Validate if the user exists and is not an admin
    const user = await User.findById(userId);
    if (!user || user.isAdmin) {
      return res.json(false);
    }

    const orderedProducts = [];
    let totalAmount = 0;

    for (const product of products) {
      const foundProduct = await Product.findById(product.productId);
      if (!foundProduct) {
        return res.json(false);
      }

      const orderedProduct = {
        productId: product.productId,
        quantity: product.quantity,
        
      };

      orderedProducts.push(orderedProduct);
      totalAmount += foundProduct.price * product.quantity;
    }

    // Create the order
    const order = new Order({
      userId,
      products: orderedProducts,
      totalAmount,
    });

    const savedOrder = await order.save();
    res.json(true);
  } catch (error) {
    res.json(false);
  }
};

// Getting user details of orders
module.exports.getOrders = (req, res) => {
  const userId = req.user.id; // Assuming req.user.id is the ID of the user

  Order.find({ userId })
    .then(orders => {
      res.send(orders);
    })
    .catch(error => {
      console.error(error);
      res.status(500).send('Internal Server Error');
    });
};

// Retrieve all orders
module.exports.getAllOrders = (req, res) => {
	return Order.find({}).then(result => {
		console.log(result)
		return res.send(result);
	})
		.catch(err => res.send(err)) 
};

const Order = require("../models/Order");
const Product = require('../models/Product');
const User = require('../models/User');
const Cart = require('../models/Cart');
const auth= require('../auth');



module.exports.addToCart = async (req, res) => {
  try {
    const { userId, productId, quantity } = req.body;

    if (req.user.isAdmin) {
          return res.status(401).send(false);
        }

    const cart = await Cart.findOne({ userId });

    if (!cart) {
      cart = new Cart({ userId, products: [], totalAmount: 0 });
    }

    const existingProduct = cart.products.find(product => product.productId === productId);
    if (existingProduct) {
      existingProduct.quantity += quantity;
    } else {
      cart.products.push({ productId, quantity });
    }

    cart.totalAmount += quantity;
    
    await cart.save();

    res.send(true);
  } catch (error) {
    console.error(error);
    res.status(500).send(false);
  }
};
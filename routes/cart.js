const express = require('express');
const cartController = require('../controller/cart');
const router = express.Router();
const auth = require('../auth');
const { verify, verifyAdmin } = auth;



router.post("/addToCart", verify, cartController.addToCart);


module.exports = router;